# Adventure game

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![License: CC BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)

(Name is a work in progress.) A game inspired by the first generation of
Pokémon games and [this video][].

[this video]: https://www.youtube.com/watch?v=D9SPozgRNnQ

## Code Mirrors

- [Codeberg](https://codeberg.org/etgr/adventure-game)

- [LibreGit](https://libregit.org/etgr/adventure-game)

## Licensing

This game is licensed in two ways:

1. all the code is licensed under a [GNU General Public License (version 3 or
   later)][gplv3]; and

1. all artwork, stories and characters are licensed under a [Creative Commons
   Attribution-ShareAlike 4.0 International License][cc by-sa] (or later)

under the condition that redistribution or derivative works do not serve to
promote white supremacy or discrimination.

[gplv3]: https://www.gnu.org/licenses/gpl-3.0
[cc by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
