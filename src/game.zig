const std = @import("std");

const s = @import("c.zig").sdl;
const kbd = @import("keyboard.zig");
const ops = @import("options.zig");

const constants = @import("constants.zig");
const Player = @import("player.zig").Player;

// Handles all the game logic assuming SDL was successfully initialized and we
// have a global window and renderer.
//
// Conventions:
//
// - Controls should be parameterized with options.zig
pub const Game = struct {
    state: union(enum) {
        TitleScreen,
        Cutscene: usize,

        Quitting,
    } = .TitleScreen,

    paused: bool = false,

    player: Player,

    joystick: ?*s.SDL_Joystick = null,

    timer: std.time.Timer = undefined,

    const l = std.log.scoped(.game);

    pub fn init() !@This() {
        l.debug("Initializing the game", .{});

        l.debug("Starting timer", .{});
        _ = std.time.Timer.start() catch |err| {
            l.debug("Failed to start timer: {}", .{err});
            return err;
        };

        const player = Player.init();

        return @This(){
            .player = player,
        };
    }

    pub fn loop(self: *@This()) void {
        const renderer = @import("root").renderer;

        while (self.isRunning()) {
            self.timer.reset();

            // Clear the render to the background color

            const bg = constants.COLOR.background;
            if (s.SDL_SetRenderDrawColor(renderer, bg.r, bg.g, bg.b, 0) != 0) {
                l.err("Error changing colors: {s}", .{s.SDL_GetError()});
            }

            if (s.SDL_RenderClear(renderer) != 0) {
                l.err("Error cleaning renderer: {s}", .{s.SDL_GetError()});
                // TODO: panic?
            }

            self.handleEvents();
            self.update(self.timer.read());
            self.draw();

            // After all is done, if there's extra time, sleep until the next
            // frame.
            const ns = self.timer.read();
            if (ns < ops.options.target_frame_time_ns)
                std.time.sleep(ops.options.target_frame_time_ns - ns);

            s.SDL_RenderPresent(renderer);
        }
    }

    fn isRunning(self: @This()) bool {
        return self.state != .Quitting;
    }

    fn handleEvents(self: *@This()) void {
        var event: s.SDL_Event = undefined;

        while (s.SDL_PollEvent(&event) != 0) {
            switch (event.@"type") {
                s.SDL_QUIT => self.onQuit(),

                else => {},
            }
        }
    }

    // _ -> elapsed_time_ns
    fn update(self: *@This(), _: u64) void {
        kbd.updateState();
        self.player.update();

        if (kbd.isKeyDown("emergency_quit")) {
            l.debug("quit", .{});
            @field(self, "on" ++ "Quit")();
            return;
        }

        if (kbd.isKeyDown("toggle_fullscreen")) {
            self.toggleFullscreen();
        }
    }

    fn onQuit(self: *@This()) void {
        self.state = .Quitting;
    }

    fn toggleFullscreen(_: *@This()) void {
        const window = @import("root").window;

        if (ops.options.fullscreen_mode == null) return;

        const is_fullscreen: u32 = s.SDL_GetWindowFlags(window) &
            s.SDL_WINDOW_FULLSCREEN;

        const flag = if (is_fullscreen != 0)
            0
        else switch (ops.options.fullscreen_mode.?) {
            .real => s.SDL_WINDOW_FULLSCREEN,
            .fake => s.SDL_WINDOW_FULLSCREEN_DESKTOP,
        };

        _ = s.SDL_SetWindowFullscreen(window, @intCast(u32, flag));
    }

    fn onJoyAdded(self: *@This(), event: s.SDL_JoyDeviceEvent) void {
        l.debug("Joystick {} connected", .{event.which});

        self.joystick = s.SDL_JoystickOpen(event.which);
        if (self.joystick == null) {
            l.err("Error opening joystick: {}", .{s.SDL_GetError()});
        }

        if (s.SDL_JoystickNameForIndex(event.which)) |name| {
            l.debug("Joystick {} opened", .{std.mem.span(name)});
        } else {
            l.debug("Cannot get device name: {}", .{s.SDL_GetError()});
        }
    }

    fn onJoyRemoved(self: *@This(), _: s.SDL_JoyDeviceEvent) void {
        l.debug("Joystick {} disconnected", .{
            s.SDL_JoystickInstanceID(self.joystick),
        });

        if (self.joystick) |joy| {
            s.SDL_JoystickClose(joy);
        }
    }

    fn onJoyButtonDown(self: *@This(), event: s.SDL_JoyButtonEvent) void {
        const ctrl = ops.options.controls.controller;
        const button = event.button;

        l.debug("button: {}", .{event.button});

        if (button == ctrl.emergency_quit) {
            l.debug("controller quit", .{});
            self.state = .Quitting;
        } else if (button == ctrl.confirm) {
            l.debug("controller confirm", .{});
        } else if (button == ctrl.cancel) {
            l.debug("controller cancel", .{});
        }
    }

    fn onAxisMotion(_: *@This(), event: s.SDL_JoyAxisEvent) void {
        l.debug("axis {} {}", .{ event.axis, event.value });
    }

    fn draw(self: @This()) void {
        self.player.draw();
    }
};
