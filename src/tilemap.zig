pub const TileMap = struct {
    const Self = @This();

    rows: usize,
    cols: usize,

    pub fn init(filename: []const u8) Self {
        return Self{
            .rows = 10,
            .colw = 10,
        };
    }
};
