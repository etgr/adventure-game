const std = @import("std");
const builtin = @import("builtin");

const print = std.debug.print;
const startsWith = std.mem.startsWith;

const c = @import("c.zig");
const s = c.sdl;
const ops = @import("options.zig");
const ver = @import("version.zig");

const Game = @import("game.zig").Game;

pub const log_level: std.log.Level = switch (builtin.mode) {
    .Debug, .ReleaseSafe => .debug,
    .ReleaseFast, .ReleaseSmall => .info,
};

// Global variables. All pieces of code are expected to access them whenever
// needed via @import("root").*. These are initialized in main.

pub var window: *s.SDL_Window = undefined;
pub var renderer: *s.SDL_Renderer = undefined;

// This is just a bunch of setting up and error checking. Skip to game.zig for
// the game logic itself.
pub fn main() !void {
    // Not much you can do without an allocator.
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const alloc = &arena.allocator;

    try setupLog();
    defer if (log_to == .file) {
        log_file.?.close();
    };

    const l = std.log.scoped(.main);

    l.info("Running version {}", .{ver.VERSION});

    {
        const sl = @import("save_load.zig");
        try sl.save(alloc);
        try sl.load(alloc);
    }

    l.debug("options = {}", .{ops.options});

    // Before we begin with the game logic, we need to initialize a bunch of
    // stuff. It's nicer to do it this way only because the game logic can then
    // just assume that everything Just Works(tm).

    c.logSdlVersion();

    if (s.SDL_Init(s.SDL_INIT_EVERYTHING) != 0) {
        l.err("Failed to initialize SDL: {s}", .{s.SDL_GetError()});
        return error.SdlInit;
    }
    defer {
        l.debug("Deinitializing SDL", .{});
        s.SDL_Quit();
    }

    c.logSdlTtfVersion();

    if (s.TTF_Init() != 0) {
        l.err("Failed to initialize SDL_ttf: {s}", .{s.TTF_GetError()});
        return error.TtfInit;
    }
    defer {
        l.debug("Deinitializing SDL_ttf", .{});
        s.TTF_Quit();
    }

    l.debug("Creating the window", .{});
    window = s.SDL_CreateWindow(
        "Game",
        ops.options.window.x,
        ops.options.window.y,
        ops.options.window.w,
        ops.options.window.h,
        ops.options.window.flags,
    ) orelse {
        l.err("Failed to create the window: {s}", .{s.SDL_GetError()});
        return error.CreateWindow;
    };
    defer {
        l.debug("Destroying the window", .{});
        s.SDL_DestroyWindow(window);
    }

    l.debug("Creating the renderer", .{});
    renderer = s.SDL_CreateRenderer(
        window,
        -1,
        ops.options.renderer.flags,
    ) orelse {
        l.err("Failed to create the renderer: {s}", .{s.SDL_GetError()});
        return error.CreateRenderer;
    };
    defer {
        l.debug("Destroying the renderer", .{});
        s.SDL_DestroyRenderer(renderer);
    }

    // We're finally ready to start the game

    var game = try Game.init();
    game.loop();
}

fn openExeDir() !std.fs.Dir {
    var buffer: [std.fs.MAX_PATH_BYTES]u8 = undefined;
    const exe_dir = try std.fs.selfExeDirPath(&buffer);
    return std.fs.openDirAbsolute(exe_dir, .{});
}

// If logging to a file, these are needed.
const LOG_FILE_NAME = "log.txt";
var log_file: ?std.fs.File = null;

fn setupLog() !void {
    // Setting up logging to a file if it was requested.
    if (log_to == .file) {
        var dir = try openExeDir();
        defer dir.close();

        // If there's already a log file, just overwrite it.
        log_file = try dir.createFile(
            LOG_FILE_NAME,
            .{ .truncate = true },
        );

        // Successfully did everything, so just print the full path of the log
        // file.
        //
        // Keep in mind that realpathAlloc only handles Linux, Windows and
        // MacOS.
        var buffer: [std.fs.MAX_PATH_BYTES]u8 = undefined;
        const log_file_path = dir.realpath(LOG_FILE_NAME, &buffer);

        print("Logging to {s}\n", .{log_file_path});
    }
}

// Easily switch between logging methods.
const log_to: enum(u8) { file, stderr, stdout, _ } = switch (builtin.mode) {
    .Debug, .ReleaseSafe => .stdout,
    .ReleaseFast, .ReleaseSmall => .file,
};

// Because of the way the log function is written, one cannot simply use the
// default stderr mutex, so it needs its own.
var log_mutex = std.Thread.Mutex{};

// Which debug symbols to ignore when searching for the interesting one.
const IGNORE_SYMBOLS = [_][]const u8{
    "std.log.log",
    "std.log.scoped",
};

pub fn log(
    comptime level: std.log.Level,
    comptime scope: @TypeOf(.EnumLiteral),
    comptime format: []const u8,
    args: anytype,
) void {
    const lock = log_mutex.acquire();
    defer lock.release();

    var writer = switch (log_to) {
        .file => log_file.?.writer(),

        .stderr => std.io.getStdErr().writer(),
        .stdout => std.io.getStdOut().writer(),

        else => |dest| @compileError(@tagName(dest) ++
            " logging unimplemented"),
    };

    writer.print("[{s}] ", .{@tagName(level)}) catch return;

    var has_all_debug_info = true;

    // If it has access to debug symbols, print the symbol name; otherwise,
    // just print the scope.
    //
    // TODO: if @src() gives this information in the future, change this code.
    if (!builtin.strip_debug_info) {
        var it = std.debug.StackIterator.init(@returnAddress(), null);

        const debug_info = std.debug.getSelfDebugInfo() catch unreachable;

        const symbol_info = while (it.next()) |addr| {
            const address = addr - 1;

            const module_info = debug_info.getModuleForAddress(address) catch {
                break null;
            };
            const symbol_info = module_info.getSymbolAtAddress(address) catch {
                break null;
            };

            var should_ignore = false;

            inline for (IGNORE_SYMBOLS) |symbol| {
                if (startsWith(u8, symbol_info.symbol_name, symbol)) {
                    should_ignore = true;
                }
            }

            if (!should_ignore) break symbol_info;
        } else null;

        if (symbol_info != null and symbol_info.?.line_info != null) {
            const line_info = symbol_info.?.line_info;

            writer.print("{s}:{}: ", .{
                symbol_info.?.symbol_name,
                line_info.?.line,
            }) catch return;
        } else {
            has_all_debug_info = false;
        }
    }

    if (!has_all_debug_info) {
        writer.print("{s}: ", .{@tagName(scope)}) catch return;
    }

    writer.print(format ++ "\n", args) catch return;
}

test {
    // Test all files when testing main
    _ = @import("c.zig");
    _ = @import("constants.zig");
    _ = @import("game.zig");
    _ = @import("keyboard.zig");
    _ = @import("main.zig");
    _ = @import("math.zig");
    _ = @import("mon.zig");
    _ = @import("options.zig");
    _ = @import("player.zig");
    _ = @import("save_load.zig");
    _ = @import("testing.zig");
    _ = @import("version.zig");
}
