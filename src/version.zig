const std = @import("std");

const testing = @import("testing.zig");

const Order = std.math.Order;
const VersionNum = u8;

pub const VERSION = struct {
    // The compiled version.
    //
    // Increasing any value requires a commit containing only this change,
    // appropriately tagged.
    //
    // Decreasing a value is forbidden, as it would completely break the code.
    //
    // Numbers will be increased as follows:
    //
    //     - major: significant changes in mechanics or gameplay;
    //
    //     - minor: mechanical adjustments, balancing, changes of default
    //       values, bug fixes that may break speedruns;
    //
    //     - patch: small bug fixes without significant changes.
    major: VersionNum = 0,
    minor: VersionNum = 0,
    patch: VersionNum = 0,

    // Default initialize *everything*, do not set fields in this function.
    fn init() @This() {
        return @This(){};
    }

    pub fn format(
        self: @This(),
        comptime fmt: []const u8, // TODO: into _ once zig#9220 is fixed
        _: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        _ = fmt; // TODO: see note on fmt above
        try writer.print("{}.{}.{}", .{
            self.major,
            self.minor,
            self.patch,
        });
    }

    pub fn ord(self: @This(), other: @This()) Order {
        return if (self.major < other.major)
            Order.lt
        else if (self.major > other.major)
            Order.gt
        else if (self.minor < other.minor)
            Order.lt
        else if (self.minor > other.minor)
            Order.gt
        else if (self.patch < other.patch)
            Order.lt
        else if (self.patch > other.patch)
            Order.gt
        else
            Order.eq;
    }

    // Version parsing errors are basically ParseIntErrors with extras.
    pub const ParseError = std.fmt.ParseIntError || error{
        InvalidVersion, // Not enough stuff in the version string (e.g. 5, 2.3)
        ExtraStuff, // Too much stuff in the version string (e.g. 8.9.11.23)
    };

    // Helper function to parse each part of the version string.
    fn partialParse(it: *std.mem.SplitIterator) ParseError!VersionNum {
        return std.fmt.parseInt(
            VersionNum,
            it.next() orelse return error.InvalidVersion,
            10,
        );
    }

    pub fn parse(str: []const u8) ParseError!@This() {
        var it = std.mem.split(str, ".");

        const parsed = @This(){
            .major = try partialParse(&it),
            .minor = try partialParse(&it),
            .patch = try partialParse(&it),
        };

        return if (it.next()) |_| error.ExtraStuff else parsed;
    }

    // Parse and compare a version against the compiled one.
    pub fn pOrd(self: @This(), other: []const u8) ParseError!Order {
        return self.ord(try @This().parse(other));
    }
}.init();

test "Version ordering" {
    const Version = @TypeOf(VERSION);

    { // Versions are equal
        const v1 = Version{ .major = 2, .minor = 1, .patch = 32 };
        const v2 = Version{ .major = 2, .minor = 1, .patch = 32 };

        testing.equal(v1.ord(v2), .eq);
        testing.equal(v2.ord(v1), .eq);
    }

    { // Versions differ in major level only
        const v1 = Version{ .major = 3, .minor = 1, .patch = 32 };
        const v2 = Version{ .major = 2, .minor = 1, .patch = 32 };

        testing.equal(v1.ord(v2), .gt);
        testing.equal(v2.ord(v1), .lt);
    }

    { // Versions differ in minor level only
        const v1 = Version{ .major = 2, .minor = 2, .patch = 32 };
        const v2 = Version{ .major = 2, .minor = 1, .patch = 32 };

        testing.equal(v1.ord(v2), .gt);
        testing.equal(v2.ord(v1), .lt);
    }

    // Versions differ in patch level only
    {
        const v1 = Version{ .major = 2, .minor = 1, .patch = 32 };
        const v2 = Version{ .major = 2, .minor = 1, .patch = 2 };

        testing.equal(v1.ord(v2), .gt);
        testing.equal(v2.ord(v1), .lt);
    }
}

test "Version parsing" {
    const Version = @TypeOf(VERSION);

    // Valid versions
    testing.equal(
        Version{ .major = 2, .minor = 11, .patch = 9 },
        Version.parse("2.11.9") catch unreachable,
    );
    testing.equal(
        Version{ .major = 93, .minor = 155, .patch = 13 },
        Version.parse("93.155.13") catch unreachable,
    );

    // Completely invalid versions
    //
    // These cases could be valid if the version was allowed to contain
    // hexadecimal values, but it isn't, so parsing one should raise an error.
    testing.err(error.InvalidCharacter, Version.parse("a.b.c"));
    testing.err(error.InvalidCharacter, Version.parse("a.123.29"));
    testing.err(error.InvalidCharacter, Version.parse("61.b.29"));
    testing.err(error.InvalidCharacter, Version.parse("78.2.d"));
    // Extra characters after version number
    testing.err(error.ExtraStuff, Version.parse("1.2.3.4"));
    testing.err(error.ExtraStuff, Version.parse("1.2.3.asd"));

    // Partially invalid versions
    //
    // These could be valid if the missing parts were defaulted to zero, but
    // they aren't, and so incomplete versions should raise errors.
    testing.err(error.InvalidVersion, Version.parse("2.0"));
    testing.err(error.InvalidVersion, Version.parse("21"));

    // Parse and compare versions
    testing.equal(
        (Version{ .major = 2, .minor = 1, .patch = 12 }).pOrd("2.1.12"),
        .eq,
    );
}
