const std = @import("std");

const l = std.log.scoped(.c);

pub const alloc = std.heap.c_allocator;

pub const sdl = @cImport({
    @cDefine("SDL_MAIN_HANDLED", {});
    @cInclude("SDL2/SDL.h");

    @cInclude("SDL2/SDL_ttf.h");
});

pub fn logSdlVersion() void {
    l.debug("Compiled with SDL {}.{}.{}", .{
        sdl.SDL_MAJOR_VERSION,
        sdl.SDL_MINOR_VERSION,
        sdl.SDL_PATCHLEVEL,
    });

    var ver_linked: sdl.SDL_version = undefined;
    sdl.SDL_GetVersion(&ver_linked);
    l.debug("Linking against SDL {}.{}.{}", .{
        ver_linked.major,
        ver_linked.minor,
        ver_linked.patch,
    });
}

pub fn logSdlTtfVersion() void {
    l.debug("Compiled with SDL_ttf {}.{}.{}", .{
        sdl.SDL_TTF_MAJOR_VERSION,
        sdl.SDL_TTF_MINOR_VERSION,
        sdl.SDL_TTF_PATCHLEVEL,
    });

    var ver_linked = sdl.TTF_Linked_Version();
    l.debug("Linking against SDL_ttf {}.{}.{}", .{
        ver_linked.*.major,
        ver_linked.*.minor,
        ver_linked.*.patch,
    });
}
