//! Extra testing functions building on std.testing.

const testing = @import("std").testing;

const eql = testing.expectEqual;
const eqe = testing.expectWithinEpsilon;

pub const err = testing.expectError;

/// Expanding std.testing.expectEqual to be a bit more clever with floats.
///
/// This makes sure the epsilon used in tests is consistent across all tests in
/// this program.
pub fn equal(expected: anytype, actual: @TypeOf(expected)) void {
    const FLOAT_EPSILON = 0.0001;

    const T = @TypeOf(expected);

    switch (@typeInfo(T)) {
        .Float => eqe(expected, actual, FLOAT_EPSILON),

        .Struct => |structT| {
            inline for (structT.fields) |field| {
                equal(@field(expected, field.name), @field(actual, field.name));
            }
        },

        else => eql(expected, actual),
    }
}
