const builtin = @import("builtin");

const s = @import("c.zig").sdl;

// Environmental variables
pub const ENV = struct {
    // Stuff needed to find the base directory
    pub usingnamespace switch (builtin.os.tag) {
        .linux => struct {
            pub const HOME = "HOME";
            pub const XDG_CONFIG_HOME = "XDG_CONFIG_HOME";

            pub const CONFIG_DIRNAME = ".config";
        },

        else => @compileError("Unknown enviroment: " ++
            @tagName(builtin.os.tag)),
    };

    // The directory inside the base directory where this game lives
    pub const GAMEDIR = "monster_kingdom";
};

pub const COLOR = struct {
    pub const background = s.SDL_Color{ .r = 255, .g = 255, .b = 255, .a = 0 };
};

pub const SPRITE = struct {
    pub const battle = struct {
        pub const w = 64;
        pub const h = 64;
    };

    pub const field = struct {
        pub const w = 16;
        pub const h = 16;
    };
};
