const std = @import("std");

const c = @import("c.zig");
const s = c.sdl;
const l = std.log.scoped(.keyboard);

var state: []const u8 = undefined;

/// Should be called once per frame and no more; calling SDL_PumpEvents() or
/// polling SDL events is necessary for this function to work properly.
///
/// Undefined behaviour before the first call.
pub fn updateState() void {
    var len: c_int = undefined;
    const new_state = s.SDL_GetKeyboardState(&len);

    state = new_state[0..@intCast(usize, len)];
}

/// Checks if a given key was pressed when the state was last updated.
///
/// The key is expected to be a field of options.controls.keyboard.
pub fn isKeyDown(comptime key: []const u8) bool {
    const kbd = @import("options.zig").options.controls.keyboard;

    const key_code = @field(kbd, key);
    const key_scancode = s.SDL_GetScancodeFromKey(key_code);
    const key_idx = @intCast(usize, key_scancode);

    return state[key_idx] != 0;
}
