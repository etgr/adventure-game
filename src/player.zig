const std = @import("std");

const kbd = @import("keyboard.zig");
const Vec2D = @import("math.zig").Vec2D;

const l = std.log.scoped(.player);

pub const Player = struct {
    const Self = @This();

    pos: Vec2D,
    vel: Vec2D = Vec2D.zero(),

    pub fn init() Self {
        return Self{
            .pos = Vec2D.init(29, 10),
        };
    }

    pub fn update(self: *Self) void {
        self.vel = Vec2D.zero();

        if (kbd.isKeyDown("up")) {
            l.debug("up", .{});
            self.vel.y -= 1;
        }

        if (kbd.isKeyDown("down")) {
            l.debug("down", .{});
            self.vel.y += 1;
        }

        if (kbd.isKeyDown("left")) {
            l.debug("left", .{});
            self.vel.x -= 1;
        }

        if (kbd.isKeyDown("right")) {
            l.debug("right", .{});
            self.vel.x += 1;
        }

        if (kbd.isKeyDown("confirm")) {
            l.debug("confirm", .{});
        }

        if (kbd.isKeyDown("cancel")) {
            l.debug("cancel", .{});
        }

        if (self.vel.magSqr() > 0) {
            self.vel.normalize();
            self.pos.add(self.vel);
        }
    }

    pub fn draw(self: Self) void {
        const s = @import("c.zig").sdl;
        const r = @import("root").renderer;

        const rect = self.pos.intoSdlRect(10, 10);

        _ = s.SDL_SetRenderDrawColor(r, 255, 0, 0, 0);
        _ = s.SDL_RenderFillRect(r, &rect);
    }
};
