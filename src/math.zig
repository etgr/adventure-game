const std = @import("std");

const s = @import("c.zig").sdl;

const testing = @import("testing.zig");

// Calculate the position corresponding to a percentage between the start and
// the end values. The percentage should be given as an absolute value between
// zero and one.
//
// If the percentage is negative, start is returned; if it is greater than one,
// the end is returned.
//
//                 lerp(start, end, amount)
//                 <-|->
//     start --------+------------------- end
//                   |
//       0   --------+-------------------  1
//       |         <-|->                   |
//       |         amount                  |
//       |                                 |
//      <- lerp = start                    |
//                              lerp = end ->
pub fn lerp(start: anytype, end: @TypeOf(start), amount: f32) @TypeOf(start) {
    const T = @TypeOf(start);
    const Float = f32; // Used when conversion from an int is necessary.

    return if (amount < 0)
        start
    else if (amount > 1)
        end
    else switch (@typeInfo(T)) {
        .Int => blk: {
            const endp = @intToFloat(Float, end);
            const startp = @intToFloat(Float, start);

            break :blk @floatToInt(T, amount * (endp - startp) + startp);
        },

        .Float => amount * (end - start) + start,

        else => @compileError("Unimplemented for " ++ @typeName(T)),
    };
}

test "lerping" {
    // Return start when the amount is negative
    testing.equal(@as(f32, 11), lerp(@as(f32, 11), 132, -0.5));
    // Return end when the amount is bigger than one
    testing.equal(@as(f32, 132), lerp(@as(f32, 11), 132, 1.5));

    // Lerp an integer interval
    testing.equal(@as(u32, 10), lerp(@as(u32, 0), 100, 0.1));
    testing.equal(@as(u32, 30), lerp(@as(u32, 0), 100, 0.3));
    testing.equal(@as(u32, 70), lerp(@as(u32, 0), 100, 0.7));
    testing.equal(@as(u32, 90), lerp(@as(u32, 0), 100, 0.9));

    // Lerp an f32 interval
    testing.equal(@as(f32, 10), lerp(@as(f32, 0), 100, 0.1));
    testing.equal(@as(f32, 30), lerp(@as(f32, 0), 100, 0.3));
    testing.equal(@as(f32, 70), lerp(@as(f32, 0), 100, 0.7));
    testing.equal(@as(f32, 90), lerp(@as(f32, 0), 100, 0.9));
}

// Calculates how many nanoseconds should each frame take for a given FPS.
pub fn frameTime(fps: u64) u64 {
    // FPS is the amount of frames that can be draw per second; therefore, the
    // time to render said nuumber of frames is
    //
    //           frames          frames
    //     fps = ------  =>  s = ------
    //              s              fps
    //
    // we want the time of a single frame, so
    //
    //          1
    //     s = ---
    //         fps
    //
    // but we want it in ns (1 ns = 1_000_000_000 s), so
    //
    //           ns         1            1_000_000_000
    //     ------------- = ---  =>  ns = -------------
    //     1_000_000_000   fps                fps
    return 1_000_000_000 / fps;
}

test "frame time" {
    testing.equal(@as(u64, 33_333_333), frameTime(30));
    testing.equal(@as(u64, 16_666_666), frameTime(60));
}

pub const Vec2D = struct {
    const Self = @This();

    x: f32,
    y: f32,

    pub fn init(x: f32, y: f32) Self {
        return Self{
            .x = x,
            .y = y,
        };
    }

    pub fn zero() Self {
        return Self{
            .x = 0,
            .y = 0,
        };
    }

    pub fn add(self: *Self, other: Self) void {
        self.x += other.x;
        self.y += other.y;
    }

    pub fn magSqr(self: Self) f32 {
        return self.x * self.x + self.y * self.y;
    }

    pub fn mag(self: Self) f32 {
        return std.math.sqrt(self.magSqr());
    }

    pub fn divS(self: *Self, scalar: f32) void {
        if (scalar == 0) @panic("Attempting to divide by zero");

        self.x /= scalar;
        self.y /= scalar;
    }

    pub fn normalize(self: *Self) void {
        self.divS(self.mag());
    }

    // Convenience function to convert a Vec2D into an SDL_Rect; assumes it
    // points to the upper left corner of the rect.
    pub fn intoSdlRect(self: Self, width: c_int, height: c_int) s.SDL_Rect {
        return s.SDL_Rect{
            .x = @floatToInt(c_int, self.x),
            .y = @floatToInt(c_int, self.y),
            .w = width,
            .h = height,
        };
    }
};

test "Vec2D" {
    { // Creating a Vec2D
        const v = Vec2D.init(3, 14);

        testing.equal(@as(f32, 3), v.x);
        testing.equal(@as(f32, 14), v.y);
    }

    { // Creating a zero Vec2D
        const v = Vec2D.zero();

        testing.equal(@as(f32, 0), v.x);
        testing.equal(@as(f32, 0), v.y);
    }

    { // Adding a Vec2D to a Vec2D
        var v1 = Vec2D.init(1, 2);
        const v2 = Vec2D.init(2, 3);

        v1.add(v2);

        testing.equal(@as(f32, 3), v1.x);
        testing.equal(@as(f32, 5), v1.y);
    }

    { // Magnitude
        const v = Vec2D.init(3, 4);

        testing.equal(@as(f32, 25), v.magSqr());
        testing.equal(@as(f32, 5), v.mag());
    }

    { // Division by a scalar
        var v = Vec2D.init(4, 8);
        v.divS(4);

        testing.equal(@as(f32, 1), v.x);
        testing.equal(@as(f32, 2), v.y);
    }

    { // Normalization
        var v = Vec2D.init(3, 4);
        v.normalize();

        testing.equal(@as(f32, 0.6), v.x);
        testing.equal(@as(f32, 0.8), v.y);
    }
}
