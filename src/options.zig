//! One stop place for all gameplay options!
//!
//! The meat of this file is the `options` global variable, which sets the
//! default values for everything. If you're here to change some default value,
//! that's where you should do it.
//!
//! Options come in two varieties: simple and elaborated. When all you need is
//! a builtin type, say a u8, then it's there and that's it; however, when
//! there's something more to it (mostly pretty printing), a struct is defined
//! and its fields are initialized in `options`.
//!
//! This way, all default values live in a single place and the extra code is
//! kept out of the way.

const std = @import("std");
const builtin = @import("builtin");

const s = @import("c.zig").sdl;
const m = @import("math.zig");

const Allocator = std.mem.Allocator;
const String = std.ArrayList(u8);

const env = std.os.getenv;
const pjoin = std.fs.path.join;

usingnamespace @import("constants.zig").ENV;

// Gameplay options.
//
// All default should be defined here rather than in their corresponding
// structs, so as to keep them all in a single place.
pub var options = struct {
    window: WindowOptions = .{
        .x = s.SDL_WINDOWPOS_CENTERED,
        .y = s.SDL_WINDOWPOS_CENTERED,

        .w = 300,
        .h = 200,

        .flags = s.SDL_WINDOW_SHOWN,
    },

    renderer: RendererOptions = .{
        .flags = 0,
    },

    // Values to be multiplied with every damage roll *after* all other
    // modifiers are applied.
    damage_multiplier: DamageMultiplier = .{
        .player = 1,
        .enemy = 1,
    },

    // Get more or less random encounters.
    encounter_multiplier: i16 = 1,

    // How many frames to wait before printing the next character.
    text_speed: u16 = 1,

    // How many nanoseconds each frame should (ideally) take.
    //
    // Stored in nanoseconds instead of FPS to avoid computing this value every
    // frame.
    target_frame_time_ns: u64 = m.frameTime(60),

    // If null, disable fullscreen; otherwise, set its mode:
    //
    // - real: use a real fullscreen with videomode change; or
    //
    // - fake: fake fullscreen by creating a window that takes the size of the
    //   desktop
    //
    // More info at https://wiki.libsdl.org/SDL_SetWindowFullscreen.
    fullscreen_mode: ?FullScreenMode = .real,

    controls: Controls = .{
        .keyboard = .{
            .emergency_quit = s.SDLK_ESCAPE,

            .up = s.SDLK_w,
            .down = s.SDLK_s,
            .left = s.SDLK_a,
            .right = s.SDLK_d,

            .confirm = s.SDLK_j,
            .cancel = s.SDLK_k,

            .toggle_fullscreen = s.SDLK_F11,
        },

        .controller = .{
            .emergency_quit = 0,

            .confirm = 1,
            .cancel = 2,
        },
    },

    const l = std.log.scoped(.options);

    // @This() will be constructed at compile-time, so we can't try to load all
    // options here; instead, this default-initialized the struct and relies on
    // other code to fill in the values when loading a save file.
    pub fn init() @This() {
        return @This(){};
    }

    pub fn format(
        self: @This(),
        comptime fmt: []const u8, // TODO: into _ once zig#9220 is fixed
        _: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        _ = fmt; // TODO: see note on fmt above
        try writer.writeAll("{ ");

        const fields = @typeInfo(@This()).Struct.fields;
        inline for (fields) |field, i| {
            try writer.print(".{s} = {}", .{
                field.name,
                @field(self, field.name),
            });

            if (i < fields.len - 1) try writer.writeAll(", ");
        }

        try writer.writeAll(" }");
    }
}.init();

const FLAG_SEPARATOR_STR = " | ";

// A common function to be used by RendererOptions and WindowOptions.
//
// Format a single flag into the writer if it is present in the self. To help
// distinguish multiple flags, it'll insert FLAG_SEPARATOR_STR between values
// and set write_separator_before to true for easier chaining.
fn formatFlag(
    self: anytype,
    comptime flag: []const u8,
    write_separator_before: *bool,
    writer: anytype,
) !void {
    if (self.flags & @intCast(u32, @field(s, flag)) != 0) {
        if (write_separator_before.*) try writer.writeAll(FLAG_SEPARATOR_STR);

        try writer.writeAll(flag);
        write_separator_before.* = true;
    }
}

// The names of all SDL_RendererFlags.
const SDL_RENDERER_FLAGS = [_][]const u8{
    "SDL_RENDERER_SOFTWARE",
    "SDL_RENDERER_ACCELERATED",
    "SDL_RENDERER_PRESENTVSYNC",
    "SDL_RENDERER_TARGETTEXTURE",
};

const RendererOptions = struct {
    flags: u32,

    pub fn format(
        self: @This(),
        comptime fmt: []const u8, // TODO: into _ once zig#9220 is fixed
        _: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        _ = fmt; // TODO: see note on fmt above
        try writer.writeAll("{ ");

        try writer.writeAll(".flags = ");

        var written = false;
        inline for (SDL_RENDERER_FLAGS) |flag| {
            try formatFlag(self, flag, &written, writer);
        }

        if (!written) try writer.writeAll("0");

        try writer.writeAll(" }");
    }
};

// This time there are more of them.
const SDL_WINDOW_FLAGS = [_][]const u8{
    "SDL_WINDOW_FULLSCREEN",
    "SDL_WINDOW_FULLSCREEN_DESKTOP",
    "SDL_WINDOW_OPENGL",
    "SDL_WINDOW_VULKAN",
    "SDL_WINDOW_HIDDEN",
    "SDL_WINDOW_BORDERLESS",
    "SDL_WINDOW_RESIZABLE",
    "SDL_WINDOW_MINIMIZED",
    "SDL_WINDOW_MAXIMIZED",
    "SDL_WINDOW_INPUT_GRABBED",
    "SDL_WINDOW_ALLOW_HIGHDPI",
    "SDL_WINDOW_SHOWN",
    "SDL_WINDOW_RESIZABLE",
};

const WindowOptions = struct {
    x: c_int,
    y: c_int,

    w: c_int,
    h: c_int,

    flags: u32,

    fn formatFlags(self: @This(), writer: anytype) !void {
        try writer.writeAll(".flags = ");

        var written = false;
        inline for (SDL_WINDOW_FLAGS) |flag| {
            try formatFlag(self, flag, &written, writer);
        }

        if (!written) try writer.writeAll("0");
    }

    fn formatPosition(
        self: @This(),
        writer: anytype,
        comptime field: []const u8,
    ) !void {
        const field_val = @field(self, field);

        try writer.print(".{s} = ", .{field});

        // There are only three options here, so there's no need for clever
        // solutions.
        switch (field_val) {
            s.SDL_WINDOWPOS_CENTERED => try writer.writeAll(
                "SDL_WINDOWPOS_CENTERED",
            ),

            s.SDL_WINDOWPOS_UNDEFINED => try writer.writeAll(
                "SDL_WINNDOWPOS_UNDEFINED",
            ),

            else => try writer.print("{}", .{field_val}),
        }
    }

    pub fn format(
        self: @This(),
        comptime fmt: []const u8, // TODO: into _ once zig#9220 is fixed
        _: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        _ = fmt;
        try writer.writeAll("{ ");

        const fields = @typeInfo(@This()).Struct.fields;

        inline for (fields) |field, i| {
            if (std.mem.eql(u8, field.name, "flags")) {
                try self.formatFlags(writer);
            } else if (std.mem.eql(u8, field.name, "x") or
                std.mem.eql(u8, field.name, "y"))
            {
                try self.formatPosition(writer, field.name);
            } else {
                try writer.print(".{s} = {}", .{
                    field.name,
                    @field(self, field.name),
                });
            }

            if (i < fields.len - 1) try writer.writeAll(", ");
        }

        try writer.writeAll(" }");
    }
};

const DamageMultiplier = struct {
    player: i16,
    enemy: i16,

    pub fn format(
        self: @This(),
        comptime fmt: []const u8, // TODO: into _ once zig#9220 is fixed
        _: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        _ = fmt; // TODO: see note on fmt above
        try writer.writeAll("{ ");

        const fields = @typeInfo(@This()).Struct.fields;
        inline for (fields) |field, i| {
            try writer.print(".{s} = {}", .{
                field.name,
                @field(self, field.name),
            });

            if (i < fields.len - 1) try writer.writeAll(", ");
        }

        try writer.writeAll(" }");
    }
};

pub const FullScreenMode = enum {
    real,
    fake,

    pub fn jsonStringify(
        self: @This(),
        _: std.json.StringifyOptions,
        out_stream: anytype,
    ) @TypeOf(out_stream).Error!void {
        try out_stream.writeAll("\"");
        try out_stream.writeAll(@tagName(self));
        try out_stream.writeAll("\"");
    }
};

const Controls = struct {
    keyboard: struct {
        emergency_quit: s.SDL_Keycode,

        up: s.SDL_Keycode,
        down: s.SDL_Keycode,
        left: s.SDL_Keycode,
        right: s.SDL_Keycode,

        confirm: s.SDL_Keycode,
        cancel: s.SDL_Keycode,

        toggle_fullscreen: s.SDL_Keycode,

        pub fn format(
            self: @This(),
            comptime fmt: []const u8, // TODO: into _ once zig#9220 is fixed
            _: std.fmt.FormatOptions,
            writer: anytype,
        ) !void {
            _ = fmt; // TODO: see note on fmt above
            try writer.writeAll("{ ");

            const fields = @typeInfo(@This()).Struct.fields;
            inline for (fields) |field, i| {
                try writer.print(".{s} = {}", .{
                    field.name,
                    @field(self, field.name),
                });

                if (i < fields.len - 1) try writer.writeAll(", ");
            }

            try writer.writeAll(" }");
        }
    },

    controller: struct {
        emergency_quit: u8,

        confirm: u8,
        cancel: u8,
    },

    pub fn format(
        self: @This(),
        comptime fmt: []const u8, // TODO: into _ once zig#9220 is fixed
        _: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        _ = fmt; // TODO: see note on fmt above
        try writer.writeAll("{ ");

        const fields = @typeInfo(@This()).Struct.fields;
        inline for (fields) |field, i| {
            try writer.print(".{s} = {}", .{
                field.name,
                @field(self, field.name),
            });

            if (i < fields.len - 1) try writer.writeAll(", ");
        }

        try writer.writeAll(" }");
    }
};
