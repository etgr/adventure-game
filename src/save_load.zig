const std = @import("std");

const s = @import("c.zig").sdl;

const ops = @import("options.zig");

const Alloc = std.mem.Allocator;
const VERSION = @import("version.zig").VERSION;

const allocPrint = std.fmt.allocPrint;

// Finds the canonical path where the save files live.
//
// Varies a lot between OSs, so this is just a dispatcher for specialized
// functions.
pub fn findSaveDir(alloc: *Allocator) ![]u8 {
    const l = std.log.scoped(.findConfigDir);

    l.debug("Figuring out directories", .{});

    return switch (builtin.os.tag) {
        .linux => findSaveDirLinux(alloc),

        else => @compileError("Unimplemented for " ++
            @tagName(builtin.os.tag)),
    };
}

fn findSaveDirLinux(alloc: *Allocator) ![]u8 {
    return if (env(XDG_CONFIG_HOME)) |xdg_dir|
        try pjoin(alloc, &[_][]const u8{
            xdg_dir,
            GAMEDIR,
        })
    else if (env(HOME)) |home_dir|
        try pjoin(alloc, &[_][]const u8{
            home_dir,
            CONFIG_DIRNAME,
            GAMEDIR,
        })
    else
        @panic(
            \\Couldn't figure out where the files are supposed to be in your
            \\system. Is $HOME set?
        );
}

// What will actually be written to/read from a save file.
//
// New fields must be kept in the order they were added to the save files, and
// all fields aside from version are required to have default values; this way,
// an old, incompatible save file can be loaded into this struct and the new
// fields can be manually set.
const SaveState = struct {
    const Self = @This();

    // Version

    version: []const u8,

    // Options

    fullscreen_mode: ?ops.FullScreenMode = .real,

    controls: struct {
        keyboard: struct {
            emergency_quit: []const u8 = "",

            up: []const u8 = "",
            down: []const u8 = "",
            left: []const u8 = "",
            right: []const u8 = "",

            confirm: []const u8 = "",
            cancel: []const u8 = "",

            toggle_fullscreen: []const u8 = "",
        } = .{},
    } = .{},

    pub fn currentState(alloc: *Alloc) !Self {
        return Self{
            .version = try allocPrint(alloc, "{}", .{VERSION}),

            .fullscreen_mode = ops.options.fullscreen_mode,

            .controls = .{
                .keyboard = .{
                    .emergency_quit = try keyToStr(alloc, "emergency_quit"),

                    .up = try keyToStr(alloc, "up"),
                    .down = try keyToStr(alloc, "down"),
                    .left = try keyToStr(alloc, "left"),
                    .right = try keyToStr(alloc, "right"),

                    .confirm = try keyToStr(alloc, "confirm"),
                    .cancel = try keyToStr(alloc, "cancel"),

                    .toggle_fullscreen = try keyToStr(alloc, "toggle_fullscreen"),
                },
            },
        };
    }

    pub fn deinit(self: Self, alloc: *Alloc) void {
        // Weirdly enough, does the right thing even if self was created with
        // currentState(), as long as the same allocator is used.
        std.json.parseFree(Self, self, .{ .allocator = alloc });
    }
};

// A rough upper limit on the save file size.
//
// Cannot really estimate it based on the SaveState struct due to the
// unknowable size of pointers.
const SAVE_FILE_MAX_BYTES: usize = 1_000;

pub fn save(alloc: *Alloc) !void {
    const save_state = try SaveState.currentState(alloc);
    defer save_state.deinit(alloc);

    // TODO: findSaveDir/openSaveDir
    const save_dir = std.fs.cwd();

    // TODO: select a save file
    const save_file = try save_dir.createFile("sav1.json", .{
        .truncate = true,
    });
    defer save_file.close();

    try std.json.stringify(save_state, .{
        .whitespace = .{ .indent = .Tab },
    }, save_file.writer());
}

pub fn load(alloc: *Alloc) !void {
    @setEvalBranchQuota(2000);
    const save_file = blk: {
        // TODO: findSaveDir/openSaveDir
        const save_dir = std.fs.cwd();

        // TODO: select a save file
        const save_file = try save_dir.openFile("sav1.json", .{});
        defer save_file.close();

        const contents = try save_file.readToEndAlloc(
            alloc,
            SAVE_FILE_MAX_BYTES,
        );

        break :blk contents;
    };
    defer alloc.free(save_file);

    var ts = std.json.TokenStream.init(save_file);
    const loaded_state = try std.json.parse(SaveState, &ts, .{
        .allocator = alloc,
    });
    defer std.json.parseFree(SaveState, loaded_state, .{
        .allocator = alloc,
    });

    ops.options.controls.keyboard = .{
        .emergency_quit = keyFromStr(loaded_state.controls.keyboard.emergency_quit),

        .up = keyFromStr(loaded_state.controls.keyboard.up),
        .down = keyFromStr(loaded_state.controls.keyboard.down),
        .left = keyFromStr(loaded_state.controls.keyboard.left),
        .right = keyFromStr(loaded_state.controls.keyboard.right),

        .confirm = keyFromStr(loaded_state.controls.keyboard.confirm),
        .cancel = keyFromStr(loaded_state.controls.keyboard.cancel),

        .toggle_fullscreen = keyFromStr(loaded_state.controls.keyboard.toggle_fullscreen),
    };
}

fn keyToStr(alloc: *Alloc, comptime key: []const u8) ![]const u8 {
    const key_code = @field(ops.options.controls.keyboard, key);
    const key_name = s.SDL_GetKeyName(key_code);

    const str = try allocPrint(alloc, "{s}", .{key_name});

    return str;
}

// The maximum number of characters needed to represent any given key as a
// string.
const MAX_KEY_LEN: usize = 30;

// A C-string representing the printable name of some key.
const KeyNameC = [MAX_KEY_LEN:0]u8;

fn keyFromStr(str: []const u8) s.SDL_Keycode {
    const c_str: KeyNameC = blk: {
        var c_str: KeyNameC = undefined;
        var i: usize = 0;
        while (i < str.len) : (i += 1) c_str[i] = str[i];

        c_str[i] = 0;

        break :blk c_str;
    };

    const key = s.SDL_GetKeyFromName(&c_str);

    std.debug.assert(key != s.SDLK_UNKNOWN);
    return key;
}
