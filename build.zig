const std = @import("std");
const builtin = @import("builtin");

const Alloc = std.mem.Allocator;
const Builder = std.build.Builder;
const Process = std.ChildProcess;
const Dir = std.fs.Dir;

const p = std.debug.print;
const cwd = std.fs.cwd;

// The name of the folder inside the cache path where the C libraries will be
// downloaded and built.
const C_LIBS_FOLDER_NAME = "clibs";
// The name of the directory inside each library folder where stuff will be
// compiled to.
const CMAKE_BUILD_DIR_NAME = "build";

const CLibDep = struct {
    name: []const u8,
    hg_url: []const u8,
    tag: []const u8,
};

var c_lib_deps = [_]CLibDep{
    .{
        .name = "SDL2",
        .hg_url = "http://hg.libsdl.org/SDL",
        .tag = "release-2.0.12",
    },
    .{
        .name = "SDL2_ttf",
        .hg_url = "http://hg.libsdl.org/SDL_ttf",
        .tag = "release-2.0.15",
    },
};

pub fn build(b: *Builder) void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const alloc = &gpa.allocator;

    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const exe = b.addExecutable("adventure-game", "src/main.zig");
    exe.setTarget(target);
    exe.setBuildMode(mode);

    exe.linkLibC();

    // By default, link against system libraries, however, optionally download
    // and compile the necessary C libs.
    const compile_libs = b.option(
        bool,
        "compile-libs",
        "Download and compile the necessary C libraries instead of relying " ++
            "on the system (requires external tools)",
    ) orelse false;

    for (c_lib_deps) |*lib| {
        if (b.option([]const u8, lib.name, "Checkout a specific tag")) |tag| {
            lib.tag = tag;
        }
    }

    const fresh_compile = b.option(
        bool,
        "fresh-compile",
        "Whether to recompile from scratch the libraries if there's " ++
            "already a build folder present or not",
    ) orelse false;

    const make_jobs = b.option(
        u8,
        "make-jobs",
        "The number of jobs make can run simmultaniously",
    ) orelse 1;

    if (compile_libs) {
        compileLibs(b, alloc, fresh_compile, target, make_jobs);
    } else {
        for (c_lib_deps) |lib| {
            exe.linkSystemLibrary(lib.name);
        }
    }

    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}

fn compileLibs(
    b: *Builder,
    alloc: *Alloc,
    fresh_compile: bool,
    target: std.zig.CrossTarget,
    make_jobs: u8,
) void {
    // Cache dir should exist by now
    var cache_root = cwd().openDir(b.cache_root, .{}) catch unreachable;
    defer cache_root.close();

    // Create the custom dir inside the cache
    var libs_root = makeAndOpen(cache_root, C_LIBS_FOLDER_NAME);
    defer libs_root.close();

    for (c_lib_deps) |lib| {
        var lib_dir = hgCloneAndCheckout(alloc, lib, libs_root);
        defer lib_dir.close();

        cmakeCompileLib(
            alloc,
            lib_dir,
            fresh_compile,
            target.os_tag orelse builtin.os.tag,
            make_jobs,
        );
    }
}

fn makeAndOpen(dir: Dir, subpath: []const u8) Dir {
    dir.makeDir(subpath) catch |err| switch (err) {
        error.PathAlreadyExists => {},

        else => unreachable,
    };

    return dir.openDir(subpath, .{}) catch unreachable;
}

fn hgCloneAndCheckout(alloc: *Alloc, lib: CLibDep, dir: Dir) Dir {
    var just_cloned = false;

    // The clone will fail if there's already a copy of the repository there
    var lib_dir = dir.openDir(lib.name, .{}) catch |err| switch (err) {
        error.FileNotFound => blk: {
            p("Cloning {s}...", .{lib.name});
            hgClone(alloc, dir, lib.hg_url, lib.name);
            p(" ok\n", .{});

            just_cloned = true;
            break :blk dir.openDir(lib.name, .{}) catch unreachable;
        },

        else => unreachable,
    };

    // No need to update the local copy that was just cloned
    if (!just_cloned) {
        p("Local copy of {s} found, updating...", .{lib.name});
        hgPullAndUpdate(alloc, lib_dir, lib.hg_url);
        p(" ok\n", .{});
    }

    // Switch to the specifiied tag
    p("Checking out {s}...", .{lib.tag});
    hgUpdate(alloc, lib_dir, lib.tag);
    p(" ok\n", .{});

    return lib_dir;
}

fn panicOnProcessFailure(res: Process.ExecResult) void {
    switch (res.term) {
        .Exited => |code| if (code != 0) {
            p(" fail\n{s}", .{res.stderr});
            @panic("Failed to clone with mercurial");
        },

        else => {},
    }
}

fn hgClone(
    alloc: *Alloc,
    dir: Dir,
    url: []const u8,
    subpath: []const u8,
) void {
    panicOnProcessFailure(Process.exec(.{
        .allocator = alloc,
        .cwd_dir = dir,
        .argv = &[_][]const u8{ "hg", "clone", url, subpath },
    }) catch unreachable);
}

fn hgPullAndUpdate(alloc: *Alloc, dir: Dir, url: []const u8) void {
    // Pull and update local files in one command
    panicOnProcessFailure(Process.exec(.{
        .allocator = alloc,
        .cwd_dir = dir,
        .argv = &[_][]const u8{ "hg", "pull", "-u", url },
    }) catch unreachable);
}

fn hgUpdate(alloc: *Alloc, dir: Dir, tag: []const u8) void {
    panicOnProcessFailure(Process.exec(.{
        .allocator = alloc,
        .cwd_dir = dir,
        .argv = &[_][]const u8{ "hg", "update", tag },
    }) catch unreachable);
}

fn cmakeCompileLib(
    alloc: *Alloc,
    dir: Dir,
    fresh_compile: bool,
    os_tag: std.Target.Os.Tag,
    make_jobs: u8,
) void {
    if (fresh_compile) {
        // Easy, just delete the whole tree
        dir.deleteTree(CMAKE_BUILD_DIR_NAME) catch unreachable;
    }

    // Get the dir even if it was removed just a few lines back.
    var build_dir = dir.openDir(
        CMAKE_BUILD_DIR_NAME,
        .{},
    ) catch |err| switch (err) {
        error.FileNotFound => makeAndOpen(dir, CMAKE_BUILD_DIR_NAME),

        else => unreachable,
    };
    defer build_dir.close();

    switch (os_tag) {
        .linux => cmakeCompileLibLinux(alloc, build_dir, make_jobs),

        else => {
            p("Don't know how to use cmake to build for {s}\n", .{
                @tagName(os_tag),
            });
            @panic("Unknown platform");
        },
    }
}

// While inside the build folder
//
// CC='zig cc' ../configure
// make
fn cmakeCompileLibLinux(
    alloc: *Alloc,
    dir: Dir,
    make_jobs: u8,
) void {
    // Using a fresh one will loose PATH information, which is bad.
    var env = std.process.getEnvMap(alloc) catch unreachable;
    defer env.deinit();

    // Make zig cc the C compiler
    env.put("CC", "zig cc") catch unreachable;

    p("Configuring...", .{});

    panicOnProcessFailure(Process.exec(.{
        .allocator = alloc,
        .cwd_dir = dir,
        .env_map = &env,
        .argv = &[_][]const u8{"../configure"},
        //.argv = &[_][]const u8{ "../configure", "--prefix=" },
    }) catch unreachable);
    p(" ok\n", .{});

    const make_jobs_str = std.fmt.allocPrint(
        alloc,
        "--jobs={}",
        .{make_jobs},
    ) catch unreachable;

    p("Compiling (this will take a while)...", .{});
    panicOnProcessFailure(Process.exec(.{
        .allocator = alloc,
        .cwd_dir = dir,
        .argv = &[_][]const u8{ "make", make_jobs_str },
    }) catch unreachable);
    p(" ok\n", .{});

    // TODO: make install to zig-cache/bin?
}
