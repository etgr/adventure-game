- [ ] save files
	- [ ] saving
		- [x] version
		- [x] controls
		- [ ] options
		- [ ] map id
		- [ ] player location
	- [x] loading
		- [x] allow missing fields for older save compatibility

- [ ] tile maps

- [ ] key remapping
	- SDL_WaitEventTimeout?

- [ ] player movement
	- [x] move a square around an empty screen
	- [x] answer events by keyboard state rather than pulling events
		- [x] map keycode to scancode and vice versa
	- [x] velocity vector
	- [ ] snap that square to the nearest tile
	- [ ] lerp to target tile

- [ ] sound
	- [ ] sound effects
	- [ ] music
